void displayRun_EditShots() {
#ifdef SHOWDEBUG
  Serial.println("RUN>EDIT SHOTS");
#endif
  lcd.clear();
  lcd.home();
  lcd.print("RUN>SHOTS");

  lcd.setCursor(0,1);
  lcd.print(char(CHAR_R));

  lcd.setCursor(3,1);
  lcd.print(char(CHAR_FB));
  lcd.setCursor(5,1);
  lcd.print(char(CHAR_FFBB));

  lcd.setCursor(8,1);
  lcd.print(settings.shotNum);
}

void onButtonPressRun_EditShots(int b) {
  int smallChange = 1;
  int largeChange = 20;
  switch(b) {
    case 0:
      currentMenuPage = MENU_STATE_RUN;
      currentRunOption = OPTION_RUN_NONE;
      hasScreenChange = true;
      saveSettings();
      break;
    case 1:
      settings.shotNum += smallChange;
      hasScreenChange = true;
      break;
    case 2:
      settings.shotNum = max(1, settings.shotNum - smallChange);
      hasScreenChange = true;
      break;
    case 3:
      settings.shotNum += largeChange;
      hasScreenChange = true;
      break;
    case 4:
      settings.shotNum = max(1, settings.shotNum - largeChange);
      hasScreenChange = true;
      break;
    case 5:
    default:
      // run button does nothing on this page
      break;
  }
}
