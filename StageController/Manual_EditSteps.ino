

void displayManual_EditSteps() {
  lcd.clear();
  lcd.home();
#ifdef SHOWDEBUG
  Serial.println("MANUAL>EDIT STEPS");
#endif
  if (isEditingLargeStep) {
    lcd.print("Manual>Lg Step");
  } else {
    lcd.print("Manual>Sm Step");
  }

  lcd.setCursor(0,1);
  lcd.print(char(CHAR_R));
  lcd.setCursor(15,1);
  lcd.print(char(CHAR_UD));

  lcd.setCursor(3,1);
  lcd.print(char(CHAR_FB));
  lcd.setCursor(5,1);
  lcd.print(char(CHAR_FFBB));

  lcd.setCursor(8,1);
  if (isEditingLargeStep) {
    lcd.print(settings.largeStep);
  } else {
    lcd.print(settings.smallStep);
  }
}

void onButtonPressManual_EditSteps(int b) {
  int smallChange = 1;
  int largeChange = 20;
  switch(b) {
    case 0:
      currentMenuPage = MENU_STATE_MANUAL;
      hasScreenChange = true;
      saveSettings();
      break;
    case 1:
      if (isEditingLargeStep) {
        settings.largeStep = min(200, settings.largeStep + smallChange);
      } else {
        settings.smallStep = min(200, settings.smallStep + smallChange);
      }
      hasScreenChange = true;
      break;
    case 2:
      if (isEditingLargeStep) {
        settings.largeStep = max(1, settings.largeStep - smallChange);
      } else {
        settings.smallStep = max(1, settings.smallStep - smallChange);
      }
      hasScreenChange = true;
      break;
    case 3:
      if (isEditingLargeStep) {
        settings.largeStep = min(200, settings.largeStep + largeChange);
      } else {
        settings.smallStep = min(200, settings.smallStep + largeChange);;
      }
      hasScreenChange = true;
      break;
    case 4:
      if (isEditingLargeStep) {
        settings.largeStep = max(1, settings.largeStep - largeChange);
      } else {
        settings.smallStep = max(1, settings.smallStep - largeChange);
      }
      hasScreenChange = true;
      break;
    case 5:
      isEditingLargeStep = !isEditingLargeStep;
      hasScreenChange = true;
      break;
    default:
      break;
  }
}
