const char VERSION_NUM[] = "0.3.5";

// menu states
typedef enum _menu_states {
  MENU_STATE_CHARACTERTEST,
  MENU_STATE_HOME,
  MENU_STATE_RUN,
  MENU_STATE_RUN_EDIT_STEPS,
  MENU_STATE_RUN_EDIT_SHOTS,
  MENU_STATE_RUN_EDIT_CAMERADELAY,
  MENU_STATE_RUN_EDIT_FLASHDELAY,
  MENU_STATE_RUNNING,
  MENU_STATE_MANUAL,
  MENU_STATE_MANUAL_EDIT_STEPS,
  MENU_STATE_POSITION,
  MENU_STATE_TESTING
} menu_states;

typedef enum _option_states_home {
  OPTION_HOME_NONE,             // main screen
  OPTION_HOME_RUN,
  OPTION_HOME_MANUAL,
  OPTION_HOME_POSITION,
  OPTION_HOME_TESTING
} option_states_home;

typedef enum _option_states_run {
  OPTION_RUN_NONE,              // info screen
  OPTION_RUN_STEPS,
  OPTION_RUN_SHOTS,
  OPTION_RUN_CAMERAON,
  OPTION_RUN_CAMERADELAY,
  OPTION_RUN_FLASHON,
  OPTION_RUN_FLASHDELAY
} option_states_run;

// values
menu_states currentMenuPage = MENU_STATE_HOME;
option_states_home currentHomeOption = OPTION_HOME_NONE;
option_states_run currentRunOption = OPTION_RUN_NONE;

#define EEPROM_ADDRESS 32
#define EEPROM_BASE 32
#define EEPROM_MAXWRITES 20
#define SETTINGS_VERSION "ss1"
struct StageSettings {
  bool cameraEnabled;
  int cameraDelay;              // ms
  bool flashEnabled;
  int flashDelay;               // ms
  int stepsPerShot;             // stepper motor steps
  int shotNum;                  // number of shots total in a run
  //// manual
  // TODO: add ability for settings to control if RUN goes forward or backward
  byte smallStep;               // left up/down arrows
  byte largeStep;               // right up/down arrows
  char configVersion[4];        // basic error checking
} settings = {
  true,                         // cameraEnabled
  1000,                         // cameraDelay
  true,                         // flashEnabled
  300,                          // flashDelay
  8,                            // stepsPerShot
  40,                           // shotNum
  4,                            // smallStep
  20,                           // largeStep
  SETTINGS_VERSION             // basic error checking
};
//bool cameraEnabled = true;
//int cameraDelay = 1000;       // ms
//bool flashEnabled = true;
//int flashDelay = 300;         // ms
//int stepsPerShot = 8;         // stepper motor steps
//int shotNum = 40;             // number of shots total in a run
//// manual
//byte smallStep = 1;           // left up/down arrows
//byte largeStep = 8;           // right up/down arrows

//// position
short rotationCount = 2;      // how many rotations (value * stepsPerRevolution)
                              // can be negative for backward motion
int currentShot = 1;          // current shot in a run
bool hasScreenChange = true;
bool isShutterOpen = false;
bool isEditingLargeStep = false;
bool isRunning = false;
bool hasStepped = false;      // has moved stepper in this step
bool hasShot = false;         // has taken photo
long startTime = 0;
long lastShotTime = 0;

#define STEPPER_DELAY 2
#define POSTFLASH_DELAY 250

#define debounceDelay 50
#define BTN1_MOVE 1
#define BTN20_MOVE 8
#define BTN200_MOVE 400

// button initialization
#define NUMBTN 6
byte BTNS[NUMBTN] = { 6, 7, 5, 2, 4, 3 };
int BTN_STEP[NUMBTN] = { BTN200_MOVE, BTN20_MOVE, BTN1_MOVE, -BTN1_MOVE, -BTN20_MOVE, -BTN200_MOVE };
byte BTN_STATE[NUMBTN];
byte BTN_LAST_STATE[NUMBTN];
long lastDebounceTime[NUMBTN];

// Set the pins on the I2C chip used for LCD connections:
//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
// NOTE I2C pins: SDA:A4  SCL:A5
#define I2C_ADDR    0x3F
#define BACKLIGHT_PIN     3
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7
LiquidCrystal_I2C  lcd(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,
                       D5_pin,D6_pin,D7_pin,BACKLIGHT_PIN,POSITIVE);

// LED pin info
#define LED 13
int LED_STATE = HIGH;

// stepper motor init
#define STEPPER_PIN_1 9
#define STEPPER_PIN_2 10
#define STEPPER_PIN_3 11
#define STEPPER_PIN_4 12
#define stepsPerRevolution 200  // change this to fit the number of steps per revolution
Stepper myStepper(stepsPerRevolution, STEPPER_PIN_1, STEPPER_PIN_2, STEPPER_PIN_3, STEPPER_PIN_4);

// Camera, Flash, and Status control
#define CAMERA_PIN A0           // same as pin 13?
#define FLASH_PIN A1            // same as pin 14?
#define STATUS_LED_PIN 8        // HIGH for green 'running', LOW for red 'idle'

#define FLASH_ON_TIME 50       // ms

/*********
 * Version history:
 *  0.3.x         added EEPROM
 *  0.2.x         added LCD menu
 *  0.1.x         manual only with hardcoded step values
 */

// special characters 0 .. 7
// 0 : UD     up and down arrow               - scroll up or down
// 1 : D      down arrow in lower half        - scroll down
// 2 : R      circle arrow                    - back to previous menu
// 3 : FB     >< single arrow top/bottom      - slow step forward/backward
// 4 : FFBB   >><< double arrow top/bottom    - fast step forward/backward
// 5 : PN     +/-                             - toggle positive/negative
// 6 : E      ... ellipses                    - options, edit
// 7 : G      > right arrow                   - go, run


#define CHAR_UD 0
const byte _CHAR_UD[8] = {
  0b00100,
  0b01010,
  0b10001,
  0b00000,
  0b00000,
  0b10001,
  0b01010,
  0b00100
};
#define CHAR_D 1
const byte _CHAR_D[8] = {
  0b00000,
  0b00000,
  0b00000,
  0b00000,
  0b00000,
  0b10001,
  0b01010,
  0b00100
};
#define CHAR_R 2
const byte _CHAR_R[8] = {
  0b00000,
  0b10110,
  0b11001,
  0b11101,
  0b00001,
  0b10001,
  0b01110,
  0b00000
};
#define CHAR_FB 3
const byte _CHAR_FB[8] = {
  0b00010,
  0b00001,
  0b00010,
  0b00000,
  0b00000,
  0b00001,
  0b00010,
  0b00001
};
#define CHAR_FFBB 4
const byte _CHAR_FFBB[8] = {
  0b10010,
  0b01001,
  0b10010,
  0b00000,
  0b00000,
  0b01001,
  0b10010,
  0b01001
};
#define CHAR_PN 5
const byte _CHAR_PN[8] = {
  0b00000,
  0b00100,
  0b01110,
  0b00100,
  0b00000,
  0b00000,
  0b01110,
  0b00000
};
#define CHAR_E 6
const byte _CHAR_E[8] = {
  0b00000,
  0b00000,
  0b00000,
  0b00000,
  0b00000,
  0b00000,
  0b10101,
  0b00000
};
#define CHAR_G 7
const byte _CHAR_G[8] = {
  0b01000,
  0b01100,
  0b01110,
  0b01111,
  0b01110,
  0b01100,
  0b01000,
  0b00000
};
