#include <Arduino.h>

#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include <Stepper.h>
#include <EEPROMex.h>

#define USESERIAL
#ifdef USESERIAL
#define SHOWDEBUG
#endif

#include <StageController.h>

void setup() {
#ifdef USESERIAL
  // init serial communication
  Serial.begin(9600);
#endif

  // init EEPROM
  while (!EEPROM.isReady()) { delay(1); }
  EEPROM.setMemPool(EEPROM_BASE, EEPROMSizeNano);
  EEPROM.setMaxAllowedWrites(EEPROM_MAXWRITES);
  loadSettings();

  // init lcd
  lcd.begin (16,2);
  lcd.setBacklight(HIGH);
  lcd.clear();
  lcd.home();

  lcd.createChar(CHAR_UD,   (byte*)_CHAR_UD);
  lcd.createChar(CHAR_D,    (byte*)_CHAR_D);
  lcd.createChar(CHAR_R,    (byte*)_CHAR_R);
  lcd.createChar(CHAR_FB,   (byte*)_CHAR_FB);
  lcd.createChar(CHAR_FFBB, (byte*)_CHAR_FFBB);
  lcd.createChar(CHAR_PN,   (byte*)_CHAR_PN);
  lcd.createChar(CHAR_E,    (byte*)_CHAR_E);
  lcd.createChar(CHAR_G,    (byte*)_CHAR_G);

  // init buttons
  for (int i=0; i<NUMBTN; i++) {
    // initial state should be HIGH because we're using internal pullup resistors,
    // a pushed state will send the pin LOW
    BTN_STATE[i] = HIGH;
    BTN_LAST_STATE[i] = HIGH;
    lastDebounceTime[i] = 0;

    pinMode(BTNS[i], INPUT_PULLUP);
  }

  // init info status LED
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LED_STATE);

  // init run status LED
  pinMode(STATUS_LED_PIN, OUTPUT);
  digitalWrite(STATUS_LED_PIN, LOW);

  // init shutter control pin
  pinMode(CAMERA_PIN, OUTPUT);
  digitalWrite(CAMERA_PIN, LOW);

  // init flash control pin
  pinMode(FLASH_PIN, OUTPUT);
  digitalWrite(FLASH_PIN, LOW);

  // init stepper
  myStepper.setSpeed(60);           // make this an option?
}

void loop() {
  LED_STATE = LOW;
  char incomingByte = 0;

#ifdef USESERIAL
  // check for serial input, which are a button alternative for testing
  if (Serial.available() > 0)
  {
    incomingByte = Serial.read();
  }
  switch( incomingByte )
  {
    case '1':
      onButtonPress(0);
      break;
    case '2':
      onButtonPress(1);
      break;
    case '3':
      onButtonPress(2);
      break;
    case '4':
      onButtonPress(3);
      break;
    case '5':
      onButtonPress(4);
      break;
    case '6':
      onButtonPress(5);
      break;
    default:
      break;
  }
#endif

  // int rr = digitalRead(2);
  // Serial.print("BTN4 ");
  // if (rr == HIGH) {
  //   Serial.println("HIGH");
  // } else {
  //   Serial.println("LOW");
  // }
  // delay(200);

  // check for button presses
  for (int i=0; i<NUMBTN; i++) {
    int reading = digitalRead(BTNS[i]);

    if (reading != BTN_LAST_STATE[i]) {
      // update last debounce time for this button if the state has changed
      lastDebounceTime[i] = millis();
    }

    if ((millis() - lastDebounceTime[i]) > debounceDelay) {
      // state has remained the same for longer than delay value
      if (reading != BTN_STATE[i]) {
        // reading is different than stored state
        BTN_STATE[i] = reading;

        if (reading == LOW) {   // set buttons to use internal pullup, so LOW is 'pushed'
          onButtonPress(i);
        }
      }
    }

    // update last button state for next comparison
    BTN_LAST_STATE[i] = reading;
  }

  if (isRunning) {
    ProcessRunning();
  }

  // draw current lcd screen
  if (hasScreenChange) {
    drawCurrentScreen();
    hasScreenChange = false;
  }

  // update status led
  digitalWrite(LED, LED_STATE);
}

void loadSettings() {
  char testSettings[4];
  int readOffset = EEPROM_ADDRESS + sizeof(StageSettings) - 4;

  // compare with SETTINGS_VERSION
  if (EEPROM.read(readOffset + 0) == SETTINGS_VERSION[0] &&
      EEPROM.read(readOffset + 1) == SETTINGS_VERSION[1] &&
      EEPROM.read(readOffset + 2) == SETTINGS_VERSION[2]) {
    // yes! looks like our stuff....
    EEPROM.readBlock(EEPROM_ADDRESS, settings);
#ifdef SHOWDEBUG
    Serial.println("EEPROM load");
#endif
  } else {
#ifdef SHOWDEBUG
    Serial.println("EEPROM empty");
#endif
  }
}

// should trigger whenever user returns from an edit screen:
// HOME_STATE_RUN_EDIT_STEPS
// HOME_STATE_RUN_EDIT_SHOTS
// HOME_STATE_RUN_EDIT_CAMERADELAY
// HOME_STATE_RUN_EDIT_FLASHDELAY
// HOME_STATE_MANUAL_EDIT_STEPS
// or when navigating up from a secondary currentRunOption to cover cameraEnabled or flashEnabled
// (the 'update' block command should prevent this from processing needlessly if there are no changes)
void saveSettings() {
  EEPROM.updateBlock(EEPROM_ADDRESS, settings);
#ifdef SHOWDEBUG
    Serial.println("EEPROM update");
#endif
}

void onButtonPress_old(int b) {
  LED_STATE = HIGH;
  lcd.setCursor(0,1);
  lcd.print("STEP            ");
  lcd.setCursor(6,1);
  lcd.print(BTN_STEP[b]);
  myStepper.step(BTN_STEP[b]);
}

void onButtonPress(int b) {
#ifdef SHOWDEBUG
  Serial.print("b");
  Serial.println(b);
#endif
  switch(currentMenuPage) {
#ifdef SHOWDEBUG
    case MENU_STATE_CHARACTERTEST:
      onButtonPressCharacterTest(b);
      break;
#endif
    case MENU_STATE_HOME:
      onButtonPressHome(b);
      break;
    case MENU_STATE_RUN:
      onButtonPressRun(b);
      break;
    case MENU_STATE_RUN_EDIT_STEPS:
      onButtonPressRun_EditSteps(b);
      break;
    case MENU_STATE_RUN_EDIT_SHOTS:
      onButtonPressRun_EditShots(b);
      break;
    case MENU_STATE_RUN_EDIT_CAMERADELAY:
      onButtonPressRun_EditCameraDelay(b);
      break;
    case MENU_STATE_RUN_EDIT_FLASHDELAY:
      onButtonPressRun_EditFlashDelay(b);
      break;
    case MENU_STATE_RUNNING:
      onButtonPressRunning(b);
      break;
    case MENU_STATE_MANUAL:
      onButtonPressManual(b);
      break;
    case MENU_STATE_MANUAL_EDIT_STEPS:
      onButtonPressManual_EditSteps(b);
      break;
    case MENU_STATE_POSITION:
      onButtonPressPosition(b);
      break;
    case MENU_STATE_TESTING:
      onButtonPressTesting(b);
      break;
    default:
      break;
  }
}

void drawCurrentScreen() {
  switch(currentMenuPage) {
#ifdef SHOWDEBUG
    case MENU_STATE_CHARACTERTEST:
      displayCharacterTest();
      break;
#endif
    case MENU_STATE_HOME:
      displayHome();
      break;
    case MENU_STATE_RUN:
      displayRun();
      break;
    case MENU_STATE_RUN_EDIT_STEPS:
      displayRun_EditSteps();
      break;
    case MENU_STATE_RUN_EDIT_SHOTS:
      displayRun_EditShots();
      break;
    case MENU_STATE_RUN_EDIT_CAMERADELAY:
      displayRun_EditCameraDelay();
      break;
    case MENU_STATE_RUN_EDIT_FLASHDELAY:
      displayRun_EditFlashDelay();
      break;
    case MENU_STATE_RUNNING:
      displayRunning();
      break;
    case MENU_STATE_MANUAL:
      displayManual();
      break;
    case MENU_STATE_MANUAL_EDIT_STEPS:
      displayManual_EditSteps();
      break;
    case MENU_STATE_POSITION:
      displayPosition();
      break;
    case MENU_STATE_TESTING:
      displayTesting();
      break;
    default:
      break;
  }
}

#ifdef SHOWDEBUG
void displayCharacterTest() {
  lcd.clear();
  lcd.home();
  lcd.print("Stage Ctrl");
  lcd.setCursor(11,0);
  lcd.print(VERSION_NUM);

  // test characters
  lcd.setCursor(0,1);
  lcd.print("^");
  lcd.setCursor(1,1);
  lcd.print(char(CHAR_UD));
  lcd.setCursor(2,1);
  lcd.print(char(CHAR_D));
  lcd.setCursor(3,1);
  lcd.print(char(CHAR_R));

  lcd.setCursor(11,1);
  lcd.print(char(CHAR_FB));
  lcd.setCursor(12,1);
  lcd.print(char(CHAR_FFBB));
  lcd.setCursor(13,1);
  lcd.print(char(CHAR_PN));
  lcd.setCursor(14,1);
  lcd.print(char(CHAR_E));
  lcd.setCursor(15,1);
  lcd.print(char(CHAR_G));
}

void onButtonPressCharacterTest(int b) {
}
#endif
