void displayRun_EditSteps() {
#ifdef SHOWDEBUG
  Serial.println("RUN>EDIT STEPS");
#endif
  lcd.clear();
  lcd.home();
  lcd.print("RUN>STEPS");

  lcd.setCursor(0,1);
  lcd.print(char(CHAR_R));

  lcd.setCursor(3,1);
  lcd.print(char(CHAR_FB));
  lcd.setCursor(5,1);
  lcd.print(char(CHAR_FFBB));

  lcd.setCursor(8,1);
  lcd.print(settings.stepsPerShot);
}

void onButtonPressRun_EditSteps(int b) {
  int smallChange = 1;
  int largeChange = 20;
  switch(b) {
    case 0:
      currentMenuPage = MENU_STATE_RUN;
      currentRunOption = OPTION_RUN_NONE;
      hasScreenChange = true;
      saveSettings();
      break;
    case 1:
      settings.stepsPerShot += smallChange;
      hasScreenChange = true;
      break;
    case 2:
      settings.stepsPerShot = max(1, settings.stepsPerShot - smallChange);
      hasScreenChange = true;
      break;
    case 3:
      settings.stepsPerShot += largeChange;
      hasScreenChange = true;
      break;
    case 4:
      settings.stepsPerShot = max(1, settings.stepsPerShot - largeChange);
      hasScreenChange = true;
      break;
    case 5:
    default:
      // run button does nothing on this page
      break;
  }
}
