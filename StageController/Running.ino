

void displayRunning() {
  lcd.clear();
  lcd.home();
  lcd.print("RUN");
  lcd.setCursor(5,0);
  if (settings.cameraEnabled) {
    lcd.print("C");
  } else {
    lcd.print("c");
  }
  lcd.setCursor(7,0);
  if (settings.flashEnabled) {
    lcd.print("F");
  } else {
    lcd.print("f");
  }
  lcd.setCursor(9,0);
  lcd.print(currentShot);
  lcd.setCursor(12,0);
  lcd.print("/");
  lcd.setCursor(13,0);
  lcd.print(settings.shotNum);

  lcd.setCursor(0,1);
  lcd.print("Running... STOP");
  lcd.setCursor(15,1);
  lcd.print(char(CHAR_G));
}

void onButtonPressRunning(int b) {
  switch(b) {
    case 5:
#ifdef SHOWDEBUG
      Serial.println("RUN: stop");
#endif
      doStop();
      break;
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    default:
      // inactive buttons on this page
      break;
  }
}

void ProcessRunning() {
  long currentTime = millis();
  if (!hasStepped && !hasShot && currentTime - lastShotTime > STEPPER_DELAY) {
    doRunStep();
  }
  if (hasStepped && !hasShot && !isShutterOpen && currentTime - lastShotTime > settings.cameraDelay) {
    doOpenShutter();
  }
  if (hasStepped && !hasShot && isShutterOpen && currentTime - lastShotTime > settings.cameraDelay + settings.flashDelay) {
    doFlash();
    doCloseShutter();
    hasShot = true;
  }
  if (hasStepped && hasShot && currentTime - lastShotTime > settings.cameraDelay + settings.flashDelay + POSTFLASH_DELAY) {
    if (currentShot == settings.shotNum) {
      doStop();
    } else {
      doRun();
    }
  }
}
