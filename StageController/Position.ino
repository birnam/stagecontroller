

void displayPosition() {
  lcd.clear();
  lcd.home();
#ifdef SHOWDEBUG
  Serial.println("POSITION");
#endif
  lcd.print("POSITION     rot");
  if (rotationCount < 10 && rotationCount > -10) {
    lcd.setCursor(11,0);
    if (rotationCount > 0) {
      lcd.print("^");
    } else {
      lcd.print(char(CHAR_D));
    }
    lcd.setCursor(12,0);
    lcd.print(abs(rotationCount));
  } else {
    lcd.setCursor(10,0);
    if (rotationCount > 0) {
      lcd.print("^");
    } else {
      lcd.print(char(CHAR_D));
    }
    lcd.setCursor(11,0);
    lcd.print(abs(rotationCount));
  }

  lcd.setCursor(0,1);
  lcd.print(char(CHAR_R));
  lcd.setCursor(13,1);
  lcd.print("GO");
  lcd.setCursor(15,1);
  lcd.print(char(CHAR_G));

  lcd.setCursor(4,1);
  lcd.print(char(CHAR_FB));
  lcd.setCursor(8,1);
  lcd.print(char(CHAR_PN));
}

void onButtonPressPosition(int b) {
  switch(b) {
    case 0:
      currentMenuPage = MENU_STATE_HOME;
      currentHomeOption = OPTION_HOME_NONE;
      hasScreenChange = true;
      break;
    case 1:
      if (rotationCount == -1) {
        rotationCount = 1;
      } else {
        rotationCount = min(200, rotationCount + 1);
      }
      hasScreenChange = true;
      break;
    case 2:
      if (rotationCount == 1) {
        rotationCount = -1;
      } else {
        rotationCount = max(-200, rotationCount - 1);
      }
      hasScreenChange = true;
      break;
    case 3:
      rotationCount = abs(rotationCount);
      hasScreenChange = true;
      break;
    case 4:
      rotationCount = 0 - abs(rotationCount);
      hasScreenChange = true;
      break;
    case 5:
      doPosition();
      hasScreenChange = true;
      break;
    default:
      break;
  }
}
