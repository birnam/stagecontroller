

void displayTesting() {
  lcd.clear();
  lcd.home();
#ifdef SHOWDEBUG
  Serial.println("TESTING");
#endif
  lcd.print("TESTING");
  lcd.setCursor(10,0);
  if (isShutterOpen) {
    lcd.print("SHTR O");
  } else {
    lcd.print("SHTR *");
  }

  lcd.setCursor(0,1);
  lcd.print(char(CHAR_R));

  lcd.setCursor(3,1);
  lcd.print(char(CHAR_FB));
  lcd.setCursor(5,1);
  lcd.print("CAM");
  lcd.setCursor(9,1);
  lcd.print("FLSH");
}

void onButtonPressTesting(int b) {
  switch(b) {
    case 0:
      currentMenuPage = MENU_STATE_HOME;
      currentHomeOption = OPTION_HOME_NONE;
      hasScreenChange = true;
      break;
    case 1:
      doTestForward();
      hasScreenChange = true;
      break;
    case 2:
      doTestBackward();
      hasScreenChange = true;
      break;
    case 3:
      doTestOpenShutter();
      hasScreenChange = true;
      break;
    case 4:
      doTestCloseShutter();
      hasScreenChange = true;
      break;
    case 5:
      doTestFlash();
      hasScreenChange = true;
    default:
      // run button does nothing on this screen
      break;
  }
}
