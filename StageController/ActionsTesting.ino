void doTestForward() {
#ifdef SHOWDEBUG
  Serial.println("Test f");
#endif
  doManual(40, true);
}

void doTestBackward() {
#ifdef SHOWDEBUG
  Serial.println("Test b");
#endif
  doManual(40, false);
}

void doTestOpenShutter() {
#ifdef SHOWDEBUG
  Serial.println("Test ShtrO");
#endif
  doOpenShutter();
}

void doTestCloseShutter() {
#ifdef SHOWDEBUG
  Serial.println("Test ShtrC");
#endif
  doCloseShutter();
}

void doTestFlash() {
#ifdef SHOWDEBUG
  Serial.println("Test Flsh");
#endif
  doFlash();
}
