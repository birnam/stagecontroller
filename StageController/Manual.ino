

void displayManual() {
  lcd.clear();
  lcd.home();
#ifdef SHOWDEBUG
  Serial.println("MANUAL");
#endif
  lcd.print("Manual");
  lcd.setCursor(7,0);
  lcd.print(char(CHAR_FB));
  lcd.setCursor(8,0);
  lcd.print(settings.smallStep);
  lcd.setCursor(12,0);
  lcd.print(char(CHAR_FFBB));
  lcd.setCursor(13,0);
  lcd.print(settings.largeStep);

  lcd.setCursor(0,1);
  lcd.print(char(CHAR_R));
  lcd.setCursor(15,1);
  lcd.print(char(CHAR_E));

  lcd.setCursor(4,1);
  lcd.print(char(CHAR_FB));
  lcd.setCursor(10,1);
  lcd.print(char(CHAR_FFBB));
}

void onButtonPressManual(int b) {
  switch(b) {
    case 0:
      currentMenuPage = MENU_STATE_HOME;
      currentHomeOption = OPTION_HOME_NONE;
      hasScreenChange = true;
      break;
    case 1:
      doManual(settings.smallStep, true);
      break;
    case 2:
      doManual(settings.smallStep, false);
      break;
    case 3:
      doManual(settings.largeStep, true);
      break;
    case 4:
      doManual(settings.largeStep, false);
      break;
    case 5:
      currentMenuPage = MENU_STATE_MANUAL_EDIT_STEPS;
      hasScreenChange = true;
      break;
    default:
      break;
  }
}
