

void displayRun_EditCameraDelay() {
#ifdef SHOWDEBUG
  Serial.println("RUN>EDIT CAM DELAY");
#endif
  lcd.clear();
  lcd.home();
  lcd.print("RUN>CAMERA DELAY");

  lcd.setCursor(0,1);
  lcd.print(char(CHAR_R));

  lcd.setCursor(3,1);
  lcd.print(char(CHAR_FB));
  lcd.setCursor(5,1);
  lcd.print(char(CHAR_FFBB));

  lcd.setCursor(8,1);
  lcd.print(settings.cameraDelay);
  lcd.setCursor(13,1);
  lcd.print("ms");
}

void onButtonPressRun_EditCameraDelay(int b) {
  int smallChange = 1;
  int largeChange = 50;
  switch(b) {
    case 0:
      currentMenuPage = MENU_STATE_RUN;
      currentRunOption = OPTION_RUN_NONE;
      hasScreenChange = true;
      saveSettings();
      break;
    case 1:
      settings.cameraDelay += smallChange;
      hasScreenChange = true;
      break;
    case 2:
      settings.cameraDelay = max(1, settings.cameraDelay - smallChange);
      hasScreenChange = true;
      break;
    case 3:
      settings.cameraDelay += largeChange;
      hasScreenChange = true;
      break;
    case 4:
      settings.cameraDelay = max(1, settings.cameraDelay - largeChange);
      hasScreenChange = true;
      break;
    case 5:
    default:
      // run button does nothing on this page
      break;
  }
}
