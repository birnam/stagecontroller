void displayRun() {
  lcd.clear();
  lcd.home();
  lcd.print("RUN");
  lcd.setCursor(5,0);
  if (settings.cameraEnabled) {
    lcd.print("C");
  } else {
    lcd.print("c");
  }
  lcd.setCursor(7,0);
  if (settings.flashEnabled) {
    lcd.print("F");
  } else {
    lcd.print("f");
  }
  lcd.setCursor(10,0);
  if (currentShot < 100) {
    lcd.print(" ");
    lcd.setCursor(11,0);
  } else if (currentShot < 10) {
    lcd.print("  ");
    lcd.setCursor(12,0);
  }
  lcd.print(currentShot);
  lcd.setCursor(12,0);
  lcd.print("/");
  lcd.setCursor(13,0);
  lcd.print(settings.shotNum);

  lcd.setCursor(0,1);
  lcd.print(char(CHAR_R));

  switch (currentRunOption) {
    case OPTION_RUN_STEPS:
#ifdef SHOWDEBUG
      Serial.println("RUN>OPTION_RUN_STEPS");
#endif
      lcd.setCursor(1,1);
      lcd.print(char(CHAR_UD));
      lcd.setCursor(15,1);
      lcd.print(char(CHAR_E));

      lcd.setCursor(4,1);
      if (settings.stepsPerShot < 10) {
        lcd.print(" ");
        lcd.setCursor(5,1);
      }
      lcd.print(settings.stepsPerShot);
      lcd.setCursor(7,1);
      lcd.print("steps");
      break;
    case OPTION_RUN_SHOTS:
#ifdef SHOWDEBUG
      Serial.println("RUN>OPTION_RUN_SHOTS");
#endif
      lcd.setCursor(1,1);
      lcd.print(char(CHAR_UD));
      lcd.setCursor(15,1);
      lcd.print(char(CHAR_E));

      lcd.setCursor(3,1);
      if (settings.shotNum < 100) {
        lcd.print(" ");
        lcd.setCursor(4,1);
      } else if (settings.shotNum < 10) {
        lcd.print("  ");
        lcd.setCursor(5,1);
      }
      lcd.print(settings.shotNum);
      lcd.setCursor(7,1);
      lcd.print("shots");
      break;
    case OPTION_RUN_CAMERAON:
#ifdef SHOWDEBUG
      Serial.println("RUN>OPTION_RUN_CAMERAON");
#endif
      lcd.setCursor(1,1);
      lcd.print(char(CHAR_UD));
      lcd.setCursor(15,1);
      lcd.print(char(CHAR_PN));

      lcd.setCursor(3,1);
      lcd.print("Cam On/Off");
      break;
    case OPTION_RUN_CAMERADELAY:
#ifdef SHOWDEBUG
      Serial.println("RUN>OPTION_RUN_CAMERADELAY");
#endif
      lcd.setCursor(1,1);
      lcd.print(char(CHAR_UD));
      lcd.setCursor(15,1);
      lcd.print(char(CHAR_E));

      lcd.setCursor(3,1);
      lcd.print("Cdlay ");
      lcd.setCursor(9,1);
      if (settings.cameraDelay < 1000) {
        lcd.print(" ");
        lcd.setCursor(10,1);
      } else if (settings.cameraDelay < 100) {
        lcd.print("  ");
        lcd.setCursor(11,1);
      } else if (settings.cameraDelay < 10) {
        lcd.print("   ");
        lcd.setCursor(12,1);
      }
      lcd.print(settings.cameraDelay);
      lcd.setCursor(13,1);
      lcd.print("ms");
      break;
    case OPTION_RUN_FLASHON:
#ifdef SHOWDEBUG
      Serial.println("RUN>OPTION_RUN_FLASHON");
#endif
      lcd.setCursor(1,1);
      lcd.print(char(CHAR_UD));
      lcd.setCursor(15,1);
      lcd.print(char(CHAR_PN));

      lcd.setCursor(3,1);
      lcd.print("Flsh On/Off");
      break;
    case OPTION_RUN_FLASHDELAY:
#ifdef SHOWDEBUG
      Serial.println("RUN>OPTION_RUN_FLASHDELAY");
#endif
      lcd.setCursor(1,1);
      lcd.print(char(CHAR_UD));
      lcd.setCursor(15,1);
      lcd.print(char(CHAR_E));

      lcd.setCursor(3,1);
      lcd.print("Fdlay ");
      lcd.setCursor(9,1);
      if (settings.flashDelay < 1000) {
        lcd.print(" ");
        lcd.setCursor(10,1);
      } else if (settings.flashDelay < 100) {
        lcd.print("  ");
        lcd.setCursor(11,1);
      } else if (settings.flashDelay < 10) {
        lcd.print("   ");
        lcd.setCursor(12,1);
      }
      lcd.print(settings.flashDelay);
      lcd.setCursor(13,1);
      lcd.print("ms");
      break;
    case OPTION_RUN_NONE:
    default:
#ifdef SHOWDEBUG
      Serial.println("RUN>OPTION_RUN_NONE");
#endif
      lcd.setCursor(1,1);
      lcd.print(char(CHAR_D));
      lcd.setCursor(15,1);
      lcd.print(char(CHAR_G));

      lcd.setCursor(3,1);
      lcd.print(settings.stepsPerShot);
      lcd.setCursor(6,1);
      lcd.print("steps");

      lcd.setCursor(12,1);
      lcd.print("run");
      break;
  }
}

void onButtonPressRun(int b) {
  switch(b) {
    case 0:
#ifdef SHOWDEBUG
      Serial.println("RUN: btn1");
#endif
      if (currentRunOption == OPTION_RUN_NONE) {
        // up to Home screen if we're at the default options page
        currentMenuPage = MENU_STATE_HOME;
        currentHomeOption = OPTION_HOME_NONE;
      } else {
        // up to the default options page if we're in a lower option
        currentRunOption = OPTION_RUN_NONE;
        saveSettings();
      }
      hasScreenChange = true;
      break;
    case 1:
#ifdef SHOWDEBUG
      Serial.println("RUN: up1");
#endif
      switch(currentRunOption) {
        case OPTION_RUN_STEPS:
          currentRunOption = OPTION_RUN_NONE;
          hasScreenChange = true;
          break;
        case OPTION_RUN_SHOTS:
          currentRunOption = OPTION_RUN_STEPS;
          hasScreenChange = true;
          break;
        case OPTION_RUN_CAMERAON:
          currentRunOption = OPTION_RUN_SHOTS;
          hasScreenChange = true;
          break;
        case OPTION_RUN_CAMERADELAY:
          currentRunOption = OPTION_RUN_CAMERAON;
          hasScreenChange = true;
          break;
        case OPTION_RUN_FLASHON:
          currentRunOption = OPTION_RUN_CAMERADELAY;
          hasScreenChange = true;
          break;
        case OPTION_RUN_FLASHDELAY:
          currentRunOption = OPTION_RUN_FLASHON;
          hasScreenChange = true;
          break;
        case OPTION_RUN_NONE:
        default:
          // up botton does nothing on this page
          break;
      }
      break;
    case 2:
#ifdef SHOWDEBUG
      Serial.println("RUN: down1");
#endif
      switch(currentRunOption) {
        case OPTION_RUN_NONE:
          currentRunOption = OPTION_RUN_STEPS;
          hasScreenChange = true;
          break;
        case OPTION_RUN_STEPS:
          currentRunOption = OPTION_RUN_SHOTS;
          hasScreenChange = true;
          break;
        case OPTION_RUN_SHOTS:
          currentRunOption = OPTION_RUN_CAMERAON;
          hasScreenChange = true;
          break;
        case OPTION_RUN_CAMERAON:
          currentRunOption = OPTION_RUN_CAMERADELAY;
          hasScreenChange = true;
          break;
        case OPTION_RUN_CAMERADELAY:
          currentRunOption = OPTION_RUN_FLASHON;
          hasScreenChange = true;
          break;
        case OPTION_RUN_FLASHON:
          currentRunOption = OPTION_RUN_FLASHDELAY;
          hasScreenChange = true;
          break;
        case OPTION_RUN_FLASHDELAY:
        default:
          // down botton does nothing on this page
          break;
      }
      break;
    case 5:
#ifdef SHOWDEBUG
      Serial.println("RUN: go");
#endif
      switch(currentRunOption) {
        case OPTION_RUN_STEPS:
          currentMenuPage = MENU_STATE_RUN_EDIT_STEPS;
          hasScreenChange = true;
          break;
        case OPTION_RUN_SHOTS:
          currentMenuPage = MENU_STATE_RUN_EDIT_SHOTS;
          hasScreenChange = true;
          break;
        case OPTION_RUN_CAMERAON:
          settings.cameraEnabled = !settings.cameraEnabled;
          hasScreenChange = true;
          break;
        case OPTION_RUN_CAMERADELAY:
          currentMenuPage = MENU_STATE_RUN_EDIT_CAMERADELAY;
          hasScreenChange = true;
          break;
        case OPTION_RUN_FLASHON:
          settings.flashEnabled = !settings.flashEnabled;
          hasScreenChange = true;
          break;
        case OPTION_RUN_FLASHDELAY:
          currentMenuPage = MENU_STATE_RUN_EDIT_FLASHDELAY;
          hasScreenChange = true;
          break;
        case OPTION_RUN_NONE:
        default:
          doRun();
          break;
      }
      break;
    case 3:
    case 4:
    default:
      // inactive buttons on this page
      break;
  }
}
