

void displayHome() {
  lcd.clear();
  lcd.home();
  lcd.print("Stage Ctrl");
  lcd.setCursor(11,0);
  lcd.print(VERSION_NUM);

  switch (currentHomeOption) {
    case OPTION_HOME_RUN:
#ifdef SHOWDEBUG
      Serial.println("drawing HOME>OPTION_HOME_RUN");
#endif
      lcd.setCursor(1,1);
      lcd.print(char(CHAR_UD));
      lcd.setCursor(15,1);
      lcd.print(char(CHAR_E));

      lcd.setCursor(3,1);
      lcd.print("RUN");
      break;
    case OPTION_HOME_MANUAL:
#ifdef SHOWDEBUG
      Serial.println("HOME>OPTION_HOME_MANUAL");
#endif
      lcd.setCursor(1,1);
      lcd.print(char(CHAR_UD));
      lcd.setCursor(15,1);
      lcd.print(char(CHAR_E));

      lcd.setCursor(3,1);
      lcd.print("MANUAL");
      break;
    case OPTION_HOME_POSITION:
#ifdef SHOWDEBUG
      Serial.println("HOME>OPTION_HOME_POSITION");
#endif
      lcd.setCursor(1,1);
      lcd.print(char(CHAR_UD));
      lcd.setCursor(15,1);
      lcd.print(char(CHAR_E));

      lcd.setCursor(3,1);
      lcd.print("POSITION");
      break;
    case OPTION_HOME_TESTING:
#ifdef SHOWDEBUG
      Serial.println("HOME>OPTION_HOME_TESTING");
#endif
      lcd.setCursor(1,1);
      lcd.print("^");
      lcd.setCursor(15,1);
      lcd.print(char(CHAR_E));

      lcd.setCursor(3,1);
      lcd.print("TESTING");
      break;
    case OPTION_HOME_NONE:
    default:
#ifdef SHOWDEBUG
      Serial.println("HOME>OPTION_HOME_NONE");
#endif
      lcd.setCursor(1,1);
      lcd.print(char(CHAR_D));
      break;
  }
}

void onButtonPressHome(int b) {
  switch(b) {
    case 1:
#ifdef SHOWDEBUG
      Serial.println("HOME: up1");
#endif
      switch(currentHomeOption) {
        case OPTION_HOME_RUN:
          currentHomeOption = OPTION_HOME_NONE;
          hasScreenChange = true;
          break;
        case OPTION_HOME_MANUAL:
          currentHomeOption = OPTION_HOME_RUN;
          hasScreenChange = true;
          break;
        case OPTION_HOME_POSITION:
          currentHomeOption = OPTION_HOME_MANUAL;
          hasScreenChange = true;
          break;
        case OPTION_HOME_TESTING:
          currentHomeOption = OPTION_HOME_POSITION;
          hasScreenChange = true;
          break;
        case OPTION_HOME_NONE:
        default:
          // top of menu
          break;
      }
      break;
    case 2:
#ifdef SHOWDEBUG
      Serial.println("HOME: down1");
#endif
      switch(currentHomeOption) {
        case OPTION_HOME_NONE:
          currentHomeOption = OPTION_HOME_RUN;
          hasScreenChange = true;
          break;
        case OPTION_HOME_RUN:
          currentHomeOption = OPTION_HOME_MANUAL;
          hasScreenChange = true;
          break;
        case OPTION_HOME_MANUAL:
          currentHomeOption = OPTION_HOME_POSITION;
          hasScreenChange = true;
          break;
        case OPTION_HOME_POSITION:
          currentHomeOption = OPTION_HOME_TESTING;
          hasScreenChange = true;
          break;
        case OPTION_HOME_TESTING:
        default:
          // end of menu
          break;
      }
      break;
    case 5:
#ifdef SHOWDEBUG
      Serial.println("HOME: go");
#endif
      switch(currentHomeOption) {
        case OPTION_HOME_RUN:
          currentMenuPage = MENU_STATE_RUN;
          currentHomeOption = OPTION_HOME_NONE;
          hasScreenChange = true;
          break;
        case OPTION_HOME_MANUAL:
          currentMenuPage = MENU_STATE_MANUAL;
          currentHomeOption = OPTION_HOME_NONE;
          hasScreenChange = true;
          break;
        case OPTION_HOME_POSITION:
          currentMenuPage = MENU_STATE_POSITION;
          currentHomeOption = OPTION_HOME_NONE;
          hasScreenChange = true;
          break;
        case OPTION_HOME_TESTING:
          currentMenuPage = MENU_STATE_TESTING;
          currentHomeOption = OPTION_HOME_NONE;
          hasScreenChange = true;
          break;
        case OPTION_HOME_NONE:
        default:
          // top of menu
          break;
      }
      break;
    case 0:
    case 3:
    case 4:
    default:
      // inactive buttons on this page
      break;
  }
}
