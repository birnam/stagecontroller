void doRun() {
#ifdef SHOWDEBUG
  if (isRunning) {
    Serial.println("RUNNING");
  } else {
    Serial.println("RUN");
  }
#endif
  if (!isRunning) {
    doCloseShutter();
    isRunning = true;
    currentShot = 1;
    startTime = millis();
    digitalWrite(STATUS_LED_PIN, HIGH);
  } else {
    currentShot++;
  }
  currentMenuPage = MENU_STATE_RUNNING;

  // reset for new shot
  hasStepped = false;
  hasShot = false;
  lastShotTime = millis();
  hasScreenChange = true;
}

void doStop() {
#ifdef SHOWDEBUG
  Serial.print("STOP");
#endif
  isRunning = false;
  currentShot = 1;
  doCloseShutter();
  digitalWrite(STATUS_LED_PIN, LOW);
  currentMenuPage = MENU_STATE_RUN;
  hasScreenChange = true;
}

void doManual(int steps, bool isForward) {
#ifdef SHOWDEBUG
  Serial.print("MANUAL ");
  Serial.print(steps);
  if (isForward) {
    Serial.print(" F");
  } else {
    Serial.print(" B");
  }
#endif
  if (isForward) {
    myStepper.step(steps);
  } else {
    myStepper.step(-steps);
  }
}

void doRunStep() {
#ifdef SHOWDEBUG
  Serial.print("RUNSTP ");
  Serial.println(currentShot);
#endif
  myStepper.step(settings.stepsPerShot);
  hasStepped = true;
}

void doPosition() {
#ifdef SHOWDEBUG
  Serial.print("POSITION ");
  Serial.print(abs(rotationCount));
  if (rotationCount > 0) {
    Serial.println(" rot F");
  } else {
    Serial.println(" rot B");
  }
  myStepper.step(rotationCount * stepsPerRevolution);
#endif
}

void doOpenShutter() {
#ifdef SHOWDEBUG
  Serial.println("ShtrO");
#endif
  digitalWrite(CAMERA_PIN, HIGH);
  isShutterOpen = true;
}

void doCloseShutter() {
#ifdef SHOWDEBUG
  Serial.println("ShtrC");
#endif
  digitalWrite(CAMERA_PIN, LOW);
  isShutterOpen = false;
}

void doFlash() {
#ifdef SHOWDEBUG
  Serial.println("Flash on");
#endif
  digitalWrite(FLASH_PIN, HIGH);
  delay(FLASH_ON_TIME);
  digitalWrite(FLASH_PIN, LOW);
  delay(1);
#ifdef SHOWDEBUG
  Serial.println("Flash off");
#endif
}
